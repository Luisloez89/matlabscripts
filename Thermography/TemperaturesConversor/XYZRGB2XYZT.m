%Luis L�pez Fern�ndez
%Script para la conversi�n de XYZRGB a XYZT segun la paleta de color utilizada para la generacion de la im�gen termica

clear all;
clc;


A = dlmread('OPTIMA.asc',' ');

    X1=(A(:,1))'; Y1=(A(:,2))'; Z1=(A(:,3))';
    R1=(A(:,4))'; G1=(A(:,5))'; B1=(A(:,6))';
% 
RGBA(:,1,1)=R1'/255; RGBA(:,1,2)=G1'/255; RGBA(:,1,3)=B1'/255;

%Convierto los valores RGB en valores de temperatura
m=18;% valor m�ximo de temperatura 
b=8;% valor m�nimo de temperatura
escala=1/(m-b);

%Definici�n de la paleta de color y temperatura en 16bits
map= colormap(jet(65536));

TA = single(rgb2ind(RGBA,map));

%Reescalamos la temperatura de 16bits a valor real con aproximaci�n a la
%decima de grado.
TempA=round(((TA/(65535*escala))+b)*10);
TempA=TempA/10;


RES(:,1) = A(:,1);
RES(:,2) = A(:,2);
RES(:,3) = A(:,3);
RES(:,4) = TempA;
RES = RES';
%dlmwrite('XYZT.asc',RES,'\t');
fid=fopen('XYZT.asc','w');
fprintf(fid,'%6.3f %6.3f %6.3f %3.1f \n', RES);
fclose(fid);