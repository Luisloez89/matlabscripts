%PROGRAMA PARA EXTRAER CABECERA ARCHIVO NEC,CAMBIAR PUNTOS POR COMAS Y
%TRANSFORMAR A .JPEG

clear;
clc;
close all;

ini= input('Nombre 1� termograf�a (debe ser un n�mero) = ');
fin= input('Nombre �LTIMA termograf�a (debe ser un n�mero) = ');

% [seq] = f_proces_txtNEC(ini,fin);

% Tref= input('Temperatura de referencia (medida con term�metro) = ');
% j=1;
% for i=1:size(seq,3)
%     Ttermo= seq(:,:,i);
%     [Tcorr,emis]=f_correctemis(Ttermo,Tref);
%     
%     seqcorr(j) = Tcorr;
%     j=j+1;
% 
% [seq_imagen] = f_convimagen(seq, tmax, tmin, paleta);

for k=ini:fin %nome das termograf�as(en formato texto)
knum=k;
ktext=int2str(knum);%Convierte n�mero entero(k) en string.
ktextin(1)=ktext; % donde las posiciones 1 al 3 escriben el n�mero k correspondiente
ktextin(2:5)='.txt'; % donde las posiciones 4 al 7 escriben el texto '.txt '
ktextout(1)=ktext;% donde las posiciones 1 al 3 escriben el n�mero k correspondiente
ktextout(2:9)='_mod.txt';% donde las posiciones 4 al 11 escriben el texto '_mod.txt'

infile=fopen(ktextin,'r'); %Abrimos los archivos (k correspondiente.txt) para lectura
outfile=fopen(ktextout,'w');%Abrimos los archivos de salida(k correspondiente_mod.txt)(sin encabezados ni comas)que pueden escribirse

for i =1:486
line=fgets(infile); % Lee l�nea a l�nea los archivos.txt empezando en la 1 y finalizando en la 486
 for j=1:length(line)% Lee todas las columnas 
 if line(j)==','% si encuentra una coma
     line(j)='.'; % la transforma en punto
 end
 end
 if i>=7 % para filas de la 7 en adelante
 fprintf(outfile,'%s',line); %escribe los datos en un archivo de salida(_mod.txt), con formato string a partir del archivo de entrada(.txt)
 end
 end
fclose(infile);% cerramos los archivos (k correspondiente.txt) para lectura
fclose(outfile);% cerramos los archivos de salida(k correspondiente_mod.txt)(sin encabezados ni comas)que pueden escribirse

p01=load(ktextout);% Carga fichero en variable
m=size(p01);%variable con valor el tama�o d la matriz (como son todas=,con 1 llega)

v=0;
for i=1:m(1) %recorremos la matriz por filas
    for j=1:m(2) %recorremos la matriz por columnas
       if p01(i,j)<=0;%desechamos los valores de T inferiores a 0
            p01(i,j)=v;
       end
    end
end

m=18;% valor m�ximo de temperatura 
b=8;% valor m�nimo de temperatura
escala=1/(m-b);
map= colormap(jet(65536));% jet, hot


    p01_nuevo=uint16(round(65535*(p01(:,:)-b)*escala));%Reescalamos para que los elementos de los archivos de texto var�en entre 0 y 255(256 colores=8 bits por pixel) 
%
% FUNCION PARA PASAR A RGB en base a matriz de grises y paleta de colores
    p01_nuevoRGB = ind2rgb(p01_nuevo,map);

    ktext = int2str(k);
    ktextjpg(1:3)=ktext;
    ktextjpg(4:11)='_mod.jpg';

    imwrite(p01_nuevoRGB,ktextjpg)%escribe la imagen p01_nuevo al file especificado por 'map'y en formato '_mod.jpg'

end
