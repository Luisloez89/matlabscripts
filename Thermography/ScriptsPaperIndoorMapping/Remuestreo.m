%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PROGRAMA PARA EL REMAPEO DE TEXTURAS TERMOGRAFICAS
%AUTOR: Luis L�pez Fern�ndez
%FECHA: 30/05/2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
clear all;
clc;
A=imread('Ortho_P3.tif');
%imshow(A)
subplot(2,1,1), subimage(A)

%Calculo una matriz B donde los puntos que sea necesario interpolar tendran
%valor 0
for m=1:size(A,1)
    for n=1:size(A,2)
        if (A(m,n,1)==0) && (A(m,n,2)==0) && (A(m,n,3)==0)
        B(m,n)=0; %Puntos sin textura
        else
        B(m,n)=1; %Puntos que ya poseen textura
        end
    end
end

%Convierto los valores RGB en valores de temperatura
m=15;% valor m�ximo de temperatura 
b=10;% valor m�nimo de temperatura
escala=1/(m-b);

%Definici�n de la paleta de color y temperatura en 16bits
map= colormap(jet(65536));
A2 = single(rgb2ind(A,map));

%Reescalamos la temperatura de 16bits a valor real con aproximaci�n a la
%decima de grado.
T=round(((A2/(65535*escala))+b)*10);
T=T/10;


%Bucle para recorrer la imagen y comprobar que elementos hay que incorporar
%a la mascara que realizarara la interpolaci�n
for i=1:size(B,1)
    
    for j=1:size(B,2)
        if B(i,j)==0
            if (i==1)
                if(j==1)
                Valor(1)= 0;
                Valor(2)= 0;
                Valor(3)= 0;
                Valor(4)= B(i,j+1);
                Valor(5)= B(i+1,j+1);
                Valor(6)= B(i+1,j);
                Valor(7)= 0;
                Valor(8)= 0;
                elseif (j==size(B,2))
                Valor(1)= 0;
                Valor(2)= 0;
                Valor(3)= 0;
                Valor(4)= 0;
                Valor(5)= 0;
                Valor(6)= B(i+1,j);
                Valor(7)= B(i+1,j-1);
                Valor(8)= B(i,j-1);
                else
                Valor(1)= 0;
                Valor(2)= 0;
                Valor(3)= 0;
                Valor(4)= B(i,j+1);
                Valor(5)= B(i+1,j+1);
                Valor(6)= B(i+1,j);
                Valor(7)= B(i+1,j-1);
                Valor(8)= B(i,j-1);
                end
            elseif (i==size(B,1))
                if(j==1)
                Valor(1)= 0;
                Valor(2)= B(i-1,j);
                Valor(3)= B(i-1,j+1);
                Valor(4)= B(i,j+1);
                Valor(5)= 0;
                Valor(6)= 0;
                Valor(7)= 0;
                Valor(8)= 0;
                elseif (j==size(B,2))
                Valor(1)= B(i-1,j-1);
                Valor(2)= B(i-1,j);
                Valor(3)= 0;
                Valor(4)= 0;
                Valor(5)= 0;
                Valor(6)= 0;
                Valor(7)= 0;
                Valor(8)= 0;
                else
                Valor(1)= B(i-1,j-1);
                Valor(2)= B(i-1,j);
                Valor(3)= B(i-1,j+1);
                Valor(4)= B(i,j+1);
                Valor(5)= 0;
                Valor(6)= 0;
                Valor(7)= 0;
                Valor(8)= B(i,j-1);
                end
            else
                if(j==1)
                Valor(1)= 0;
                Valor(2)= B(i-1,j);
                Valor(3)= B(i-1,j+1);
                Valor(4)= B(i,j+1);
                Valor(5)= B(i+1,j+1);
                Valor(6)= B(i+1,j);
                Valor(7)= 0;
                Valor(8)= 0;

                elseif (j==size(B,2))
                Valor(1)= B(i-1,j-1);
                Valor(2)= B(i-1,j);
                Valor(3)= 0;
                Valor(4)= 0;
                Valor(5)= 0;
                Valor(6)= B(i+1,j);
                Valor(7)= B(i+1,j-1);
                Valor(8)= B(i,j-1);

                else
                Valor(1)= B(i-1,j-1);
                Valor(2)= B(i-1,j);
                Valor(3)= B(i-1,j+1);
                Valor(4)= B(i,j+1);
                Valor(5)= B(i+1,j+1);
                Valor(6)= B(i+1,j);
                Valor(7)= B(i+1,j-1);
                Valor(8)= B(i,j-1);

                end 
            end
            
                %Adquiero los valores de la mascara de media
            if Valor(1)==1
                Valor(1)= T(i-1,j-1);
            end
            
            if Valor(2)==1
                Valor(2)= T(i-1,j);
            end
            
             if Valor(3)==1
                Valor(3)= T(i-1,j+1);
             end
            
              if Valor(4)==1
                Valor(4)= T(i,j+1);
              end
            
               if Valor(5)==1
                Valor(5)= T(i+1,j+1);
               end
            
                if Valor(6)==1
                Valor(6)= T(i+1,j);
                end
            
                 if Valor(7)==1
                Valor(7)= T(i+1,j-1);
                 end
            
                  if Valor(8)==1
                Valor(8)= T(i,j-1);
                  end
            
            
            %Calculo la textura del punto a remuestrear
            Text=0;
            l=0;
            for k=1:8
               if (Valor(k)~= 0)
                  Text=Text+Valor(k);
                  l=l+1;
               end
                
            end
            
            %guardo la textura en una nueva matriz con el fin de que este
            %valor ya interpolado no "contamine" otras interpolaciones.
            %La matriz C estara formada por los valores de temperatura de
            %los pixeles interpolados y los pixeles candidatos que no han
            %sido interpolados constaran como 1
            Text=(Text*10);
            if l>=1
            C(i,j)=(round(Text/l))/10;
            else
            C(i,j)=1;
            end
        else
            C(i,j)=1;
        end
        
    end
end


%A�ado a la imagen inicial los valores interpolados
for i=1:size(B,1)
    for j=1:size(B,2)
        if C(i,j)==1
            T2(i,j)=T(i,j);
        else
            T2(i,j)=C(i,j);
        end
    end
end


T3=uint16(((65535*(T2-b)*escala)));

TRGB =((ind2rgb(T3,map)));


%Los valores candidatos a ser interpolados que no han cumplido los
%requisitos de la interpolaci�n se pintan de negro
for i=1:size(C,1)

    for j=1:size(C,2)
        if  (B(i,j)==0) &&(C(i,j)==1)
        TRGB(i,j,1)=0;
        TRGB(i,j,2)=0;
        TRGB(i,j,3)=0;
        end
    end
end

imwrite(TRGB, 'Remuestreada.tif')
subplot(2,1,2),subimage(TRGB)
colorbar('location','East')
%figure; imshow(TRGB)
toc

