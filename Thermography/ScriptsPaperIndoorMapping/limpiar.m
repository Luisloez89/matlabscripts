%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PROGRAMAMA DISE�ADO PARA LIMPIAR NUBES DE PUNTOS ELIMINANDO PUNTOS SIN
%TEXTURA.
%AUTOR: Luis L�pez Fern�ndez
%FECHA: 30/05/2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
%Cargamos la nube
nube_in=load('132_1510.asc');
%Genero los ficheros de salida
fich_buenos='nube_limp.asc';
ficherosalOK=fopen(fich_buenos,'w');

fich_elim='nube_elim.asc';
ficherosalELIM=fopen(fich_elim,'w');
%Comprobamos las coordenadas RGB y eliminamos el punto si es preciso.
R=255;
G=255;
B=255;

eliminados=0;
for i=1:size(nube_in,1)
    if ((nube_in(i,4)==R) &(nube_in(i,5)==G) & (nube_in(i,6)==B))
       eliminados=eliminados+1;
       fprintf(ficherosalELIM, '%g %g %g %g %g %g',nube_in(i,1), nube_in(i,2), nube_in(i,3), nube_in(i,4), nube_in(i,5), nube_in(i,6));
       fprintf(ficherosalELIM, '\n');
        continue;
    else
        fprintf(ficherosalOK, '%g %g %g %g %g %g',nube_in(i,1), nube_in(i,2), nube_in(i,3), nube_in(i,4), nube_in(i,5), nube_in(i,6));
        fprintf(ficherosalOK, '\n');
    end
end
sprintf('Elimninados %g puntos',eliminados)
fclose(ficherosalOK);
fclose(ficherosalELIM);

