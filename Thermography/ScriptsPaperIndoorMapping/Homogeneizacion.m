%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CALCULO DE LOS PUNTOS COMUNES ENTRE DOS NUBES DE PUNTOS Y HOMOGENEIZACION
%DE PUNTOS COMUNES.
%AUTOR: Luis L�pez Fern�ndez
%FECHA: 30/05/2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
clear all;
tic %Control de tiempo
%Cargo la nube de puntos
A=load('135_1510.asc');
B=load('136_1510.asc');

%Convierto coordenadas XYZ RGB a vectores

    X1=(A(:,1))'; Y1=(A(:,2))'; Z1=(A(:,3))';
    R1=(A(:,4))'; G1=(A(:,5))'; B1=(A(:,6))';
    X2=(B(:,1))'; Y2=(B(:,2))'; Z2=(B(:,3))';
    R2=(B(:,4))'; G2=(B(:,5))'; B2=(B(:,6))';

%Conversion de componentes RGB a temperatura.
%Hipermatriz de 3 dimensiones para poder utilizar rgb2ind
%/255 ya que las componentes RGB deben ir de 0 a 1.
RGBA(:,1,1)=R1'/255; RGBA(:,1,2)=G1'/255; RGBA(:,1,3)=B1'/255;
RGBB(:,1,1)=R2'/255; RGBB(:,1,2)=G2'/255; RGBB(:,1,3)=B2'/255;

m=15;% valor m�ximo de temperatura 
b=10;% valor m�nimo de temperatura
escala=1/(m-b);
%Definici�n de la paleta de color y temperatura en 16bits
map= colormap(jet(65536));
TA = single(rgb2ind(RGBA,map));
TB = single(rgb2ind (RGBB,map));

%Reescalamos la temperatura de 16bits a valor real con aproximaci�n a la
%decima de grado.
TempA=round(((TA/(65535*escala))+b)*10);
TempB=round(((TB/(65535*escala))+b)*10);
TempA=TempA/10; TempB=TempB/10;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluo la diferencia entre los valores maximo y minimos de una nube para
%elegir el plano de proyeccion.
maxX1=max(X1); minX1=min(X1);
maxY1=max(Y1); minY1=min(Y1);
difX1=maxX1-minX1; difY1=maxY1-minY1;

%Utilizando la diferencia mayor para que la proyeccion sea mas adecuada
%calculo la envolvente convexa de cada nube. 
if difX1>=difY1
    k1 = convhull(X1,Z1);
    plot(X1,Z1, '.', 'markersize',10,'color','c'); 
    hold on; plot(X1(k1), Z1(k1), 'r'); 
     
    Xk1=X1(k1); Zk1=Z1(k1);
    k2 = convhull(X2,Z2);
    plot(X2,Z2, '.', 'markersize',10); 
    plot(X2(k2), Z2(k2), 'r'); 
    
    %Coordenadas de los puntos de las envolventes
    Xk1=X1(k1);  Zk1=Z1(k1);
    Xk2=X2(k2);  Zk2=Z2(k2);
    
    %Compruebo la pertenencia de cada punto de una nuve al cierre convexo de la
    %restante
    IN1=inpolygon(X1,Z1,Xk2,Zk2);
    IN2=inpolygon(X2,Z2,Xk1,Zk1);

    %Recorro la nube de puntos comprobando si cada punto pertenece al
    %cierre convexo de la otra nube. Si es asi busco semejantes.
    k=1;
    c=1; %Contador para escribir los puntos no comunes de A
    
    for i=1:length(X1)
    encontradoA=false;
        if IN1(i)== 1
            for j=1:length(X2)
                if IN2(j)==1
                    if ((X1(i)==X2(j)) && (Y1(i)==Y2(j)) && (Z1(i)==Z2(j)))
                        Xcom(k)=[X1(i)];
                        Zcom(k)=[Z1(i)];
                        T1com(k)=[TempA(i)];
                        T2com(k)=[TempB(j)];
                        %Escribiendo la matriz dentro del propio bucle se
                        %ralentiza considerablemente el proceso. Almaceno
                        %las posiciones de los puntos comunes en ambas
                        %matrices.
                        PosA(k)=i;
                        PosB(k)=j;                        
                        k=k+1;
                        encontradoA=true;
                        if encontradoA==true
                            continue
                        end
                    end     
                end
            end
        end
        if encontradoA==false
                PosC(c)=i;
                c=c+1;
        end
    end
    
d=1;%Contador para escribir los puntos no comunes de A
    for j=1:length(X2)
    encontradoB=false;
        if IN2(j)== 1
            for i=1:length(X1)
                if IN1(i)==1
                    if ((X1(i)==X2(j)) && (Y1(i)==Y2(j)) && (Z1(i)==Z2(j)))
                        encontradoB=true;
                        if encontradoB==true
                            continue
                        end
                    end
                end
            end
        end
        if encontradoB==false
                PosD(d)=j;
                d=d+1;
        end
    end
plot(Xcom,Zcom, '*', 'markersize',10,'color','r'); 
    
else
    k1 = convhull(Y1,Z1);
    plot(Y1,Z1, '.', 'markersize',10,'color','c'); 
    hold on;
    k2 = convhull(Y2,Z2);
    plot(Y2,Z2, '.', 'markersize',10); 
    %Pinto las envolventes convexas
    plot(Y1(k1), Z1(k1), 'm');
    plot(Y2(k2), Z2(k2), 'r'); 
        
    %Coordenadas de los puntos de las envolventes
    Yk1=Y1(k1); Zk1=Z1(k1);
    Yk2=Y2(k2); Zk2=Z2(k2);
    
    %Compruebo la pertenencia de cada punto de una nuve al cierre convexo de la
    %restante
    IN1=inpolygon(Y1,Z1,Yk2,Zk2);
    IN2=inpolygon(Y2,Z2,Yk1,Zk1);

    %Recorro la nube de puntos comprobando si cada punto pertenece al
    %cierre convexo de la otra nube. Si es asi busco semejantes.
    k=1;
    c=1;%Contador para escribir los puntos no comunes de A
    
    for i=1:length(Y1)
    encontradoA=false;
        if IN1(i)== 1
            for j=1:length(Y2)
                encontradoB=false;
                if IN2(j)==1
                    if ((X1(i)==X2(j)) && (Y1(i)==Y2(j)) && (Z1(i)==Z2(j)))
                        Ycom(k)=[Y1(i)];
                        Zcom(k)=[Z1(i)];
                        T1com(k)=[TempA(i)];
                        T2com(k)=[TempB(j)];                        
                        PosA(k)=i;
                        PosB(k)=j;
                        k=k+1;
                        encontradoA=true;
                        if encontradoA==true
                            continue
                        end
                    end
                end
            end
        end
        if encontradoA==false
                PosC(c)=i;
                c=c+1;
        end
    end
    
    d=1;
    for j=1:length(Y2)
    encontradoB=false;
        if IN2(j)== 1
            for i=1:length(Y1)
                if IN1(i)==1
                    if ((X1(i)==X2(j)) && (Y1(i)==Y2(j)) && (Z1(i)==Z2(j)))
                        encontradoB=true;
                        if encontradoB==true
                            continue
                        end
                    end
                end
            end
        end
        if encontradoB==false
                PosD(d)=j;
                d=d+1;
        end
    end

plot(Ycom,Zcom, '*', 'markersize',10,'color','r'); 
end

%Matriz con los puntos no comunes de A
C(:,1)=A((PosC),1); C(:,2)=A((PosC),2);
C(:,3)=A((PosC),3); C(:,4)=TempA(PosC);
%Matriz con los puntos no comunes de B
D(:,1)=B((PosD),1); D(:,2)=B((PosD),2);
D(:,3)=B((PosD),3); D(:,4)=TempB(PosD);

%Matriz de puntos comunes con las temperaturas de las dos nubes
E(:,1)=A((PosA),1); E(:,2)=A((PosA),2); E(:,3)=A((PosA),3);
E(:,4)=TempA(PosA); E(:,5)=TempB(PosB); 

%PONDERACI�N DE LOS VALORES DE T INVERSAMENTE PROPORCIONAL A SU DIST A LA
%NUBE

%Puntod centrales de cada nube para ponderar la media de temperaturas
    %inversamente segun el cuadrado de la distancia a estos.
    X1mean=mean(X1); Y1mean=mean(Y1); Z1mean=mean(Z1);
    X2mean=mean(X2); Y2mean=mean(Y2); Z2mean=mean(Z2);
   
for i=1:size(E,1)
   P1(i)=inv((sqrt(((E(i,1)-X1mean)^2)+((E(i,2)-Y1mean)^2)+((E(i,3)-Z1mean)^2))^2));
   P2(i)=inv((sqrt(((E(i,1)-X2mean)^2)+((E(i,2)-Y2mean)^2)+((E(i,3)-Z2mean)^2))^2));
   TempM(i)=(round((((E(i,4)*P1(i))+(E(i,5)*P2(i)))/(P1(i)+P2(i)))*10))/10;
end

%Sustituimos los valores de temperatura por un valor unico ponderado
E(:,4)=TempM';
E(:,5)=[];
%Construcc�on e la nube final
Nube=[C;D;E];
%REONVERTIMOS LOS VALORES DE TEMPERATURA A RGB
Temperatura=uint16(round(65535*(Nube(:,4)-b)*escala));%Reescalamos para que los elementos de los archivos de texto var�en entre 0 y 255(256 colores=8 bits por pixel) 
% FUNCION PARA PASAR A RGB en base a matriz de grises y paleta de colores
map= colormap(jet(65536));
TemperaturaRGB = round((ind2rgb(Temperatura,map))*255);

Nube(:,7)=Nube(:,4);
Nube(:,4)=TemperaturaRGB(:,1,1);
Nube(:,5)=TemperaturaRGB(:,1,2);
Nube(:,6)=TemperaturaRGB(:,1,3);

%Escribimos la nube en un fichero ASCII (X,Y,Z,R,G,B,T)
ficherosal=fopen('Nube.asc','w');
for i=1:size(Nube,1)
    fprintf(ficherosal,'%g %g %g %g %g %g %g',Nube(i,1),Nube(i,2),Nube(i,3),Nube(i,4),Nube(i,5),Nube(i,6),Nube(i,7));
    fprintf(ficherosal,'\n');
end
fclose(ficherosal);
toc %Control de tiempo
sprintf('%g Puntos en comun',size(E,1))


