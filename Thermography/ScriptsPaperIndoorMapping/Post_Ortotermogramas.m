%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PROGRAMA PARA EL POSTPROCESO DE ORTOTERMOGRAMAS Y ANALISIS ENERGETICO
%AUTOR: Luis L�pez Fern�ndez
%FECHA: 30/05/2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close ('all');
clc;
A=imread('Pared3_rem.png');

%Generamos una mascara donde almacenaremos como "true" los pixeles que no
%pertenecen a la fachada (Pixeles negros)
ContadorSup=0;
for i=1:size(A,1)
    for j=1:size(A,2)
        if A(i,j,1)==0 && A(i,j,2)==0 && A(i,j,3)==0
            B(i,j)=1;
        else
            ContadorSup=ContadorSup+1;
             B(i,j)=0;
        end
    end
end

%Eliminaci�n de la superficie correspondiente con las zonas sin interes
%Convierto los valores RGB en valores de temperatura
m=15;% valor m�ximo de temperatura
b=10;% valor m�nimo de temperatura
escala=1/(m-b);

%Definici�n de la paleta de color y temperatura en 16bits
map= colormap(jet(65536));
A2 = single(rgb2ind(A,map));

%Reescalamos la temperatura de 16bits a valor real con aproximaci�n a la
%decima de grado
T=round(((A2/(65535*escala))+b)*10);
T=T/10;

%Mostramos la Ortoimagen cargada y permitimos un primer estudio facilitando
% las coordenadas i,j y la temperatura del pixel que seleccionemos
%Para iniciar la segmentaci�n pulsamos "enter"
figure (1);
imshow(A);
title('ORTOTERMOGRAF�A','FontName','Verdana','FontSize',16,'FontWeight','Bold','Color',[0 0 1]);
xlabel('Pulsa "enter" para iniciar la segmentaci�n de las zonas de interes');
Ok=false;
x=0;
y=0;
while Ok==false
[y1,x1] = ginput(1);
x1=round(x1);
y1=round(y1);
if isempty(x1)
    Ok=true;
    
else
    if B(x1,y1)==0
    clc;
    sprintf('i= %g  j= %g  T= %g �',x1,y1,T(x1,y1))
    x=x1;
    y=y1;
    else
        clc;
    end
end
end

%Calculo la mediana de las temperaturas para generar dos ortotermogramas
%donde segmentar respectivamente las zonas de perdida de calor y los huecos
k=1;
for i=1:size(B,1)
    for j=1:size(B,2)
        if B(i,j)==0
            T2(k)=T(i,j);
            k=k+1;
        end
    end
end
mediana=median(T2);
media=mean(T2);
%Genero dos mascaras booleanas para almacenar los pixeles que corresponden
%a cada una de las dos imagenes mencionadas anteriormente

for i=1:size(T,1)
    for j=1:size(T,2)
        if B(i,j)==0 && T(i,j)<media
            %C son los pixeles de la imagen donde se encontraran las zonas
            %mas frias
           C(i,j)=1;
        else
            C(i,j)=0;
        end
        if B(i,j)==0 && T(i,j)>mediana
            %D son los pixeles de la imagen donde se encontraran las
            %ventanas
           D(i,j)=1;
        else
            D(i,j)=0;
        end
    end
end

Ok=false;
while Ok==false
%CALCULO DE LAS ZONAS FRIAS
C2=A;
%Genero la imagen de las zonas frias
for i=1:size(A,1)
    for j=1:size(A,2)
        if C(i,j)==0
            C2(i,j,:)=0;
        end
    end
end

figure (2);
imshow(C2);
title('ZONAS FR�AS','FontName','Verdana','FontSize',16,'FontWeight','Bold','Color',[0 0 1]);

%Binarizo la imagen
Coef1=input('Introduzca coeficiente de binarizaci�n [0,1]: ');
C3=im2bw(C2,Coef1);

%Elimino los puntos que no pertenecen a la zona de estuido de "zonas frias"
for i=1:size(C3,1)
    for j=1:size(C3,2)
        if C(i,j)==0
            C3(i,j)=0;
        else
            if C3(i,j)==1 
            C3(i,j)=0;
            else  C2(i,j)==0
            C3(i,j)=1;
            end
        end
    end
end

figure (3);
imshow(C3);

%Convierto en labels la imagen binaria
LabelsF=bwlabel(C3,4);
%Inicio la selecci�n de superficies
SelF=bwselect(LabelsF);

figure (4);
imshow(SelF);
title('ZONAS DE PERDIDA DE TEMPERATURA SELECCIONADAS','FontName','Verdana','FontSize',16,'FontWeight','Bold','Color',[0 0 1]);
xlabel('Punse "Enter" y elija Continuar o Repetir selecci�n');
pause;
Cont = input('�Continuar o Repetir selecci�n? C/R [C]: ', 's');
if isempty(Cont)
    Cont = 'C';
    
end
if Cont=='C'
    Ok=true;
end
end

SupPerd=0;
k=1;
for i=1:size(SelF,1)
    for j=1:size(SelF,2)
        if SelF(i,j)==1
            SupPerd=SupPerd+1;
            Tperd(k)=T(i,j);
            k=k+1;
        end
    end
end
if SupPerd ~=0
TperdMed=(round(mean(Tperd)*10))/10;
Perdidas = true;
else
    Perdidas=false;
end
close ('all');

%CALCULO DE LAS ZONAS CALIENTES (VENTANAS)
%IDEM. zonas frias.
Ok=false;
while Ok==false

D2=A;
for i=1:size(A,1)
    for j=1:size(A,2)
        if D(i,j)==0
            D2(i,j,:)=0;
        end
    end
end

figure (5);
imshow(D2)
Coef2=input('Introduzca coeficiente de binarizaci�n [0,1]: ');
D3=im2bw(D2,Coef2);
title('ZONAS DE TEMPERATURA ALTA','FontName','Verdana','FontSize',16,'FontWeight','Bold','Color',[0 0 1]);

for i=1:size(D3,1)
    for j=1:size(D3,2)
        if D(i,j)==0
            D3(i,j)=0;
        else
            if D3(i,j)==1 
            D3(i,j)=0;
            else  D2(i,j)==0
            D3(i,j)=1;
            end
        end
    end
end


figure (6);
imshow(D3);
LabelsC=bwlabel(D3,4);
SelC=bwselect(LabelsC);
figure (7);
imshow(SelC);
title('VENTANAS SELECCIONADAS','FontName','Verdana','FontSize',16,'FontWeight','Bold','Color',[0 0 1]);
xlabel('Punse "Enter" y elija Continuar o Repetir selecci�n');
pause;
Cont = input('�Continuar o Repetir selecci�n? C/R [C]: ', 's');
if isempty(Cont)
    Cont = 'C';
    
end
if Cont=='C'
    Ok=true;
end
end

SupVent=0;
close('all');
for i=1:size(SelC,1)
    for j=1:size(SelC,2)
        if SelC(i,j)==1
            SupVent=SupVent+1;
        end
    end
end

%Temperatura media del muro s�n patolog�as
if Perdidas ==true
k=1;
for i=1:size(A,1)
    for j=1:size(A,2)
        if SelF(i,j)==0 && SelC(i,j)==1
            Tmuro(k)=T(i,j);
            k=k+1;
        end
    end
end
TmuroMed=(round(mean(Tmuro)*10))/10;
end


GSD=input('Introduzca el GSD del ortotermograma (cm): ');  %Indicamos el GSD
SupPix=GSD^2;
%Con el contador obtenemos la superficie de la fachada 
SupFach=(ContadorSup*SupPix)/10000;
SupPerd=(SupPerd*SupPix)/10000;

SupVent=(SupVent*SupPix)/10000;
SupPared=SupFach-SupVent;

%Escribimos el fichero de salida con los datos calculados
ficherosal=fopen('Resultados.txt','w');
fprintf(ficherosal,'Superficie de la fachada = %g m^2 \n',SupFach);
fprintf(ficherosal,'Superficie de Pared = %g m^2 \n',SupPared);
fprintf(ficherosal,'Superficie de ventanas = %g m^2 \n',SupVent);
fprintf(ficherosal,'Superficie de perdidas = %g m^2 \n',SupPerd);

%CALCULO DE LA EFICIENCIA ENERGETICA
clc;

fin=false;
contador=1;
while fin==false
    
Cond=input('Muro:Ingrese la conductividad termica del material de la capa (W/(m*K)): ');

Esp=input('Muro:Ingrese el espesor de la capa (m): ');
Muro(contador,1)=Cond;
Muro(contador,2)=Esp;
contador=contador+1;
Cont = input('Muro:�Introducir una nueva capa? Y/N [Y]: ', 's');
if isempty(Cont)
    Cont = 'Y';
end
if Cont=='N'
    fin=true;
end
end
%Existencia de camara de aire no ventilada
Rc=0;
Cam=input('�Existe camara de aire no ventilada en el cerramiento? Y/N [Y]: ', 's');
if isempty(Cam)
    Cam = 'Y';
end
if Cam=='N'
    fin=true;
else
    Esp=input('Espesor de la c�mara de aire no ventilada (1, 2, 5 o 10 cm): ', 's');
    switch Esp
        case (1)
            Rc=0.15;
        case (2)
            Rc=0.17;
        case (5)
            Rc=0.18;
        case (10)
            Rc=0.18;
    end
end
%Resistencias termicas correspondientes a las bolsas de aire
Rsi=input('Ingrese la resistencia t�rmicas superficial correspondientes al aire interior (Rsi): ');
Rse=input('Ingrese la resistencia t�rmicas superficial correspondientes al aire exterior(Rse): ');

%Resistencias termicas correspondientes a los componenetes de la fachada.
for i=1:size(Muro,1)
    R(i)=Muro(i,2)/Muro(i,1);
end
%Resistencia total
Rt=sum(R)+Rsi+Rse+Rc;
%Transmitancia total Muro
U=inv(Rt)
if SupVent~=0
sprintf('Estudio de los huecos(Ventanas)')

FM=input('Ventanas:Ingrese el porcentage correspondiente con marco (%): ')/100;

Uhv=input('Ventanas:transmitancia t�rmica del la superficie semitransparente (W/m�K): ');
Uhm=input('Ventanas:transmitancia t�rmica del marco (W/m�K): ');

%Transmitancia total hueco
Uh=((1-FM)*Uhv)+(FM*Uhm)
else
    Uh=0;
end
%Perdidas de temperatura
Tint=input('Temperatura interior en el momento de la captura de datos (K): ')
Text=input('Temperatura exterior en el momento de la captura de datos (K): ')
%Perdida de calor en los muros
Q=U*(SupPared-SupPerd)*(Tint-Text)
%Perdida de calor en las zonas criticas
if Perdidas==true
Qp=(U*SupPerd*(Tint-Text))+(U*SupPerd*(TmuroMed-TperdMed));
else
    Qp=0;
end

%Perdida de calor en las ventanas
Qv=Uh*(Tint-Text)*SupVent

fprintf(ficherosal,'\nTransmitancia total Muro = %g W/(m^2*K)  \n',U);
fprintf(ficherosal,'Transmitancia total hueco = %g W/(m^2*K) \n\n',Uh);
fprintf(ficherosal,'Perdida de calor en el muro en buen estado = %g W \n',Q);
fprintf(ficherosal,'Perdida de calor en las zonas criticas = %g W \n',Qp);
fprintf(ficherosal,'Perdida de calor total en el muro = %g W \n',Q+Qp);
fprintf(ficherosal,'Perdida de calor en los huecos = %g W \n',Qv);
fprintf(ficherosal,'Perdida de calor en la fachada = %g W \n',Q+Qv+Qp);
fclose(ficherosal);


